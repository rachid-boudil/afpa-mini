# projet afpa-mini

mini projet dans le cadre de la formation javascript de l'afpa

# prerequis

docker, git, node, npm

## renomage

personaliser selon vos besoin

cp dist.env .env

# installation

npm install

# démarrer

docker-compose up